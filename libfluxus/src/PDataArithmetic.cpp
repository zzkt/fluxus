// Copyright (C) 2005 Dave Griffiths
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "PDataArithmetic.h"

using namespace Fluxus;

template <>
PData *AddOperator::Operate(TypedPData<float> *a, float b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{
		(*a)[i]+=b;
	}
	return NULL;
}

template <>
PData *AddOperator::Operate(TypedPData<dVector> *a, float b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{
		((*a)[i]).x+=b;
		((*a)[i]).y+=b;
		((*a)[i]).z+=b;
	}
	return NULL;
}

template <>
PData *AddOperator::Operate(TypedPData<dVector> *a, dVector b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{
		((*a)[i]).x+=b.x;
		((*a)[i]).y+=b.y;
		((*a)[i]).z+=b.z;
	}
	return NULL;
}

template <>
PData *AddOperator::Operate(TypedPData<float> *a, TypedPData<float> *b)
{
	for (unsigned int i=0; i<a->Size(); i++) (*a)[i]+=(*b)[i];
	return NULL;
}

template <>
PData *AddOperator::Operate(TypedPData<dVector> *a, TypedPData<float> *b)
{
	for (unsigned int i=0; i<a->Size(); i++) 
	{
		(*a)[i].x+=(*b)[i];
		(*a)[i].y+=(*b)[i];
		(*a)[i].z+=(*b)[i];
	}
	return NULL;
}

template <>
PData *AddOperator::Operate(TypedPData<dVector> *a, TypedPData<dVector> *b)
{
	for (unsigned int i=0; i<a->Size(); i++) (*a)[i]+=(*b)[i];
	return NULL;
}

template <>
PData *AddOperator::Operate(TypedPData<dColour> *a, float b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{
		(*a)[i].r+=b;
		(*a)[i].g+=b;
		(*a)[i].b+=b;
	}
	return NULL;
}

template <>
PData *AddOperator::Operate(TypedPData<dColour> *a, TypedPData<float> *b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{
		(*a)[i].r+=(*b)[i];
		(*a)[i].g+=(*b)[i];
		(*a)[i].b+=(*b)[i];
	}
	return NULL;
}

template <>
PData *AddOperator::Operate(TypedPData<dColour> *a, dColour b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{
		((*a)[i]).r+=b.r;
		((*a)[i]).g+=b.g;
		((*a)[i]).b+=b.b;
		((*a)[i]).a+=b.a;
	}
	return NULL;
}

template <>
PData *AddOperator::Operate(TypedPData<dColour> *a, TypedPData<dColour> *b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{	
		(*a)[i]+=(*b)[i];
	}
	return NULL;
}
////////////////////////////////////////////////////////////////////////////

template <>
PData *MultOperator::Operate(TypedPData<float> *a, float b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{	
		(*a)[i]*=b;
	}
	return NULL;
}

template <>
PData *MultOperator::Operate(TypedPData<dVector> *a, float b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{	
		(*a)[i]*=b;
	}
	return NULL;
}

template <>
PData *MultOperator::Operate(TypedPData<dColour> *a, float b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{	
		(*a)[i]*=b;
	}
	return NULL;
}

template <>
PData *MultOperator::Operate(TypedPData<dVector> *a, dVector b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{
		(*a)[i].x*=b.x;
		(*a)[i].y*=b.y;
		(*a)[i].z*=b.z;
	}
	return NULL;
}

template <>
PData *MultOperator::Operate(TypedPData<float> *a, TypedPData<float> *b)
{
	for (unsigned int i=0; i<a->Size(); i++) (*a)[i]*=(*b)[i];
	return NULL;
}

template <>
PData *MultOperator::Operate(TypedPData<dVector> *a, TypedPData<float> *b)
{
	for (unsigned int i=0; i<a->Size(); i++) 
	{
		(*a)[i]*=(*b)[i];
	}
	return NULL;
}

template <>
PData *MultOperator::Operate(TypedPData<dVector> *a, TypedPData<dVector> *b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{
		(*a)[i].x*=(*b)[i].x;
		(*a)[i].y*=(*b)[i].y;
		(*a)[i].z*=(*b)[i].z;
	}
	return NULL;
}

///////////////////////////////////////////////////////

template <>
PData *SineOperator::Operate(TypedPData<float> *a, TypedPData<float> *b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{
		(*a)[i]=sin((*b)[i]);
	}
	return NULL;
}

///////////////////////////////////////////////////////

template <>
PData *CosineOperator::Operate(TypedPData<float> *a, TypedPData<float> *b)
{
	for (unsigned int i=0; i<a->Size(); i++)
	{	
		(*a)[i]=cos((*b)[i]);	
	}
	return NULL;
}

///////////////////////////////////////////////////////

template <>
PData *ClosestOperator::Operate(TypedPData<dVector> *a, dVector b)
{
	float closestdist=999999;
	dVector closest;
	for (unsigned int i=0; i<a->Size(); i++)
	{	
		float dist = (*a)[i].dist(b);	
		if (dist<closestdist)
		{
			closestdist=dist;
			closest=(*a)[i];
		}
		
	}
	
	TypedPData<dVector> *ret = new TypedPData<dVector>;
	ret->PushBack(closest);
	return ret;
}

template <>
PData *ClosestOperator::Operate(TypedPData<dVector> *a, float b)
{
	// use the float as the index
	unsigned int index=(unsigned int)b;
	float closestdist=999999;
	dVector closest;
	for (unsigned int i=0; i<a->Size(); i++)
	{	
		if (i!=index)
		{
			float dist = (*a)[i].dist((*a)[index]);	
			if (dist<closestdist)
			{
				closestdist=dist;
				closest=(*a)[i];
			}
		}
	}
	
	TypedPData<dVector> *ret = new TypedPData<dVector>;
	ret->PushBack(closest);
	return ret;
}

