; renders all the primitives that fluxus supports

(clear)
(clear-colour (vector 0.5 0.5 0.5))
(define tex (load-texture "test.png"))

;(translate (vector -6 -3 0))
;(scale (vector 1.5 1.5 1.5))

;build-prim-label
(define fontname "Bitstream-Vera-Sans-Mono.ttf")
(define (p-label txt)
  (with-state
    (scale (vector 0.1 0.1 0.1))
    (scale (vector 0.5 0.5 0.5))
    (translate (vector -12 -20 0))
    (colour (vector 0 0 1))
    (texture 0) ; clear texture
    (build-extruded-type fontname txt 1)))

; poly cube
(texture tex)
(build-cube)
(p-label "cube")


; poly sphere
(translate (vector 1.5 0 0))
(with-state
    (scale (vector 0.5 0.5 0.5))
    (build-sphere 30 30))
(p-label "sphere")

; poly cylinder
(translate (vector 1.5 0 0))
(with-state
    (translate (vector 0 -0.5 0))
    (scale (vector 0.5 1 0.5))
    (build-cylinder 30 30))
(p-label "cylinder")

; poly plane
(translate (vector 1.5 0 0))
(build-plane)
(p-label "plane")

; poly lines
(translate (vector 1.5 -.5 0))
(with-state
    (hint-unlit)
    (with-primitive (build-ribbon 4)
        (pdata-set "p" 0 (vector 0 0 0))
        (pdata-set "p" 1 (vector 1 0 0))
        (pdata-set "p" 2 (vector 0.5 1.3 0))
        (pdata-set "p" 3 (vector 1.2 1 0))
        (pdata-set "w" 0 0.1)
        (pdata-set "w" 1 0.1)
        (pdata-set "w" 2 0.01)
        (pdata-set "w" 3 0.1)))
(translate (vector 0.5 0.5 0))
(p-label "lines")

; nurbs sphere
(translate (vector -6.5 2 0))
(texture tex)

(with-state
    (hint-points)
    (point-width 3)
    (scale (vector 0.5 0.5 0.5))
    (with-primitive (build-nurbs-sphere 10 20)
        ; tweak a vertex to prove it's curvy
        (pdata-set "p" 95 (vector -1 1 1))
        (recalc-normals 1)))
(p-label "nerbs sphere")

; nurbs plane
(translate (vector 2 0 0))
(with-state
    (scale (vector 2 2 2))
    (hint-points)
    (point-width 3)
    (scale (vector 0.5 0.5 0.5))
    (with-primitive (build-nurbs-plane 10 10)
        ; tweak a vertex to prove it's curvy
        (pdata-set "p" 40 (vector 0.4 0.4 1))
        (recalc-normals 1)))
(p-label "nerbs plane")

; particles
(translate (vector 1.5 -.5 0))
(with-state
    (hint-none)
    (hint-points)
    (point-width 3)
    (with-primitive (build-particles 100)
        ; randomise the particle positions
        (pdata-map!
            (lambda (p)
                (rndvec))
            "p")))
(translate (vector 0.5 0.5 0))
(p-label "particles")

; sprite particles
(translate (vector 1.5 0 0))
(with-state
    (point-width 10)
    ; a function to init the particle points and colours
    (hint-anti-alias)
    (with-primitive (build-particles 100)
        ; randomise the particle positions
        (pdata-map!
            (lambda (p)
                (vector (flxrnd) (flxrnd) (flxrnd)))
            "p")

        ; set the particle colours to white
        (pdata-map!
            (lambda (c)
                (vector 1 1 1))
            "c")))
(translate (vector 0.5 0 0))
(p-label "sprite particles")
    

; blobby primitive
(translate (vector 1.5 0 0))
(with-state
    (point-width 10)
    (with-primitive (build-blobby 3 (vector 20 20 20) (vector 1 1 1))
        ; randomise the influence positions
        (pdata-map!
            (lambda (p)
                (vector (flxrnd) (flxrnd) (flxrnd)))
            "p")
        ; set the radius of the influences
        (pdata-map! (lambda (s) 0.05) "s")))
(translate (vector 1 0 0))
(p-label "blobby")

; pixels primitive
(translate (vector -8.5 2 0))
(with-primitive (build-pixels 100 100)
    ; randomise the influence positions
    (pdata-map!
        (lambda (c)
            (vector 0 0 (flxrnd) 1))
        "c")
    (pixels-upload))
(p-label "pixels")

; locator primitive (ohh - interesting!)
(translate (vector 1.5 0 0))
(with-state
    (hint-origin)
    (build-locator))
(p-label "locator")
