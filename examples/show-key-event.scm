; Show special key code on screen
;
(clear)

(with-state
    ;(hint-unlit)
    ;(hint-wire)
    (scale 0.4)
    (colour (vector 1 0.5 0))
    (wire-colour (vector 0.5 0 1))
    (translate (vector -15 5 0))
    (build-extruded-type "Bitstream-Vera-Sans-Mono.ttf" "keys:" 1))

(define key-text
    (with-state
        (hint-unlit)
        (scale 0.3)
        (translate (vector -15 0 0))
        (build-type "Bitstream-Vera-Sans-Mono.ttf" "key values")))

(define (key->string key)
  (let ([o (open-output-string)])
    (print key o)
    (get-output-string o)))

(define (update-key)
    (let ([keys-special (keys-special-down)]
          [keys (map key->string (keys-down))])
        (with-primitive key-text
            (when (pdata-exists? "s")
                  ;(displayln (pdata-ref "s" 0))
                  (pdata-set! "s" 0 (format "~a, ~a" keys keys-special))
          ))))

(every-frame (update-key))
